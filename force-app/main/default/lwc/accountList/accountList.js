import { LightningElement, wire,track } from 'lwc';
import NAME_FIELD from '@salesforce/schema/Account.Name';
import REVENUE_FIELD from '@salesforce/schema/Account.AnnualRevenue';
import INDUSTRY_FIELD from '@salesforce/schema/Account.Industry';
//import getAccounts from '@salesforce/apex/AccountController.getAccounts';
import getAccountlist from '@salesforce/apex/ImperativeApexController.getAccountList'
/*const COLUMNS = [
    { label: 'Account Name', fieldName: NAME_FIELD.fieldApiName, type: 'text' },
    { label: 'Annual Revenue', fieldName: REVENUE_FIELD.fieldApiName, type: 'currency' },
    { label: 'Industry', fieldName: INDUSTRY_FIELD.fieldApiName, type: 'text' }
];*/
export default class AccountList extends LightningElement {
    //columns = COLUMNS;
    /*@wire(getAccounts)
    accounts;*/
    @track inputValue;
    @track options = [];

    handleinputChange(event){
        this.inputValue = event.target.value;
        try{
        getAccountlist({numberofRecords : this.inputValue})
            .then((result) => {
                var optionArray = [];
                console.log('inside the uccess=='+result);
                for(let record of result){
                    console.log('each record=='+record);
                    let splitvalue = record.split("$$");
                    optionArray.push({label: splitvalue[0], value : splitvalue[1]});
                    //this.options.push({label: splitvalue[0], value : splitvalue[1]});
                    for(let i = 0; i< optionArray.length; i++){
                console.log('inside the for loop'+ optionArray[i]);
                this.options.push({ label:optionArray[i].label, value : optionArray[i].value});

            }
            console.log('ooptions=='+this.options);
                }
                console.log('value of option array==', optionArray);
                console.log('value of option array==', JSON.stringify(this.options));
            })
            .catch((error) => {
                console.log('inside the error', error);
            });

            /*for(let i = 0; i< optionArray.length; i++){
                console.log('inside the for loop'+ optionArray[i]);
                this.options.push({ label:optionArray[i].label, value : optionArray[i].value});

            }
            console.log('ooptions=='+this.options);*/
        }
        catch(err){
            console.log('value of error=='+err);
        }
    }

}