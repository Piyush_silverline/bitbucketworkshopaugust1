public class AccountUtils {
    
    public static List<Account> accountsByState( String strStateAbb){
        return [SELECT Id, Name FROM Account WHERE BillingState =: strStateAbb];
    }
    
    public static void createdata(){
        createDataForPD2Test();
    }
    
    @future
    public static void createDataForPD2Test(){
        List<Account> lstAccountToInsert = new List<Account>();
        for(Integer count = 0; count <200; count++){
            Account objAccount = new Account(Name = 'TEst'+count);
            lstAccountToInsert.add(objAccount);
        }
        System.debug('account list size==>>'+lstAccountToInsert.size());
        insert lstAccountToInsert;
        List<Contact> lstContact = new List<Contact>();
        
        for(Account objAcc : lstAccountToInsert){
            for(Integer innerCount = 0; innercount <50; innerCount++){
                lstContact.add(new Contact(FirstName = 'Test', LastName = 'Con'+innercount));
            }
        }
        System.debug('Contact list size==>>'+lstContact.size());
        insert lstContact;
        List<Campaign> lstCampaign = new List<Campaign>();
        for(Contact objContact : lstContact){
            lstCampaign.add(new Campaign(Contact__c = objContact.Id, Name = 'Campaign '+objContact.Name));
        }
        System.debug('Campaign list size==>>'+lstCampaign.size());
        insert lstCampaign;
    }
}