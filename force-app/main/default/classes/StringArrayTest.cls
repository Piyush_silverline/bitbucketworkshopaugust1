/*
 * CreatedBy:- Piyush
 * Created Date:- 22nd Jan, 2019
 * JIRA :- Trailhead task
 * Trailhead link:- https://trailhead.salesforce.com/content/learn/modules/apex_database/apex_database_intro?trailmix_creator_id=00550000006Iet7AAC&trailmix_id=pd1-preparation
 */
public class StringArrayTest {

    public static List<String> generateStringArray(Integer intCount){
        List<String> lstStringToReturn = new List<String>();
        for(Integer count=0;count<intCount; count++){
            lstStringToReturn.add('Test '+count);
        }
        system.debug('size is ++'+ lstStringToReturn.size());
        return lstStringToReturn;
    }
}