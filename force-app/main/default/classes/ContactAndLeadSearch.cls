/*
 * CreatedBy:- Piyush
 * Created Date:- 22nd Jan, 2019
 * JIRA :- Trailhead task
 * Trailhead link:- https://trailhead.salesforce.com/content/learn/modules/apex_database/apex_database_sosl?trailmix_creator_id=00550000006Iet7AAC&trailmix_id=pd1-preparation
 */
public class ContactAndLeadSearch {

    public static List<List<SObject>> searchContactsAndLeads(String strLastName){
        return [FIND :strLastName IN NAME FIELDS RETURNING Contact(FirstName,LastName,Department), Lead(Company)];
                   
    }
}