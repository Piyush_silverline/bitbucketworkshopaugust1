/**
* @ClassName    : SL_EncompassUtil 
* @JIRATicket   : Encompass Integration
* @CreatedOn    : 06th November,2018
* @CreatedBy    : Banu Ponnuswamy
* @ModifiedBy   : 
* @Description  : Reusable utility functions.
*/
public class SL_EncompassUtil {
    
    //Methods queries SF and forms the personID map which will be used to match the existhing person accounts
    public static Map<String, Id> getPersonAccountMap(Set<String> lastNameList, Set<String> firstNameList, Set<Date> dobList) {
        Map <String, Id> matchPersonAccountMap = new Map <String, Id>(); //Key FIRSTNAME-LASTNAME-DOB (YYYY-MM-DD)    1956-04-10
        if(lastNameList <> NULL && lastNameList.size() > 0) {
            /*List<Account> existingAccounts = [SELECT Id, FirstName, LastName, PersonBirthdate FROM Account WHERE RecordType.Name ='Customer' AND FirstName != NULL AND PersonBirthdate!= NULL  AND LastName in :lastNameList AND FirstName in :firstNameList AND PersonBirthdate in :dobList];
            System.debug('Account list from last name--'+existingAccounts.size());
            for(Account objAcc : existingAccounts) {
                matchPersonAccountMap.put(buildDelimetedString(objAcc.FirstName.toUpperCase(),objAcc.LastName.toUpperCase(),String.valueOf(objAcc.PersonBirthdate),'-'), objAcc.Id);
            }*/
            
        }
        System.debug('matchPersonAccountMap--'+matchPersonAccountMap);
        return matchPersonAccountMap;
    }
    
    //Method queries the opportunity object with the loanIDs received on the Encompass response to check the existing LoanIDs in the system
    public static Map<String, Id> getLoanOptyIdMap(Set<String> loanIdList) {
        System.debug('Loan Size----'+loanIdList.size());
        Map <String, Id> loanOptyIdMap = new Map <String, Id>();
        for(Opportunity optyObj : [SELECT ExternalID__c,Id FROM Opportunity WHERE ExternalID__c != NULL AND ExternalID__c in : loanIdList]) {//recordtype.name = 'Mortgage Loan' AND removed by piyush since getting error from other recordtype.
            loanOptyIdMap.put(optyObj.ExternalID__c, optyObj.Id);
        }
        return loanOptyIdMap;
    }
    
    //Method builds the list of LastNames to query the existing person accounts in the SF
    public static Set<String> getNameList(List<sObject> completeAccountList, String fieldName) {
        Set<String> nameList = new Set<String>();
        for(sObject accObj : completeAccountList){
            if(accObj.get(fieldName) <> NULL)
                nameList.add((String)accObj.get(fieldName));
        }
        System.debug('--'+fieldName +' list ---'+nameList+'--SIZE--'+nameList.size());     
        return nameList;
    }
    
    //Method builds the list of LastNames to query the existing person accounts in the SF
    public static Set<Date> getDOBList(List<sObject> completeAccountList) {
        Set<Date> dobList = new Set<Date>();
        for(sObject accObj : completeAccountList){
            if(accObj.get('PersonBirthdate') <> NULL)
                dobList.add((Date)accObj.get('PersonBirthdate'));
        }
        System.debug('dobList list ---'+dobList+'--SIZE--'+dobList.size());     
        return dobList;
    }

    public static String buildDelimetedString(String str1, String str2, String str3, String strDelimeter){
        return str1+strDelimeter+str2+strDelimeter+str3;
    }

    //Method to check whether the given email is valid or not.
    public static Boolean checkEmailValidity(String strEmail){
        if(strEmail != NULL){
            String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; 
            Pattern MyPattern = Pattern.compile(emailRegex);
            Matcher MyMatcher = MyPattern.matcher(strEmail);
           // System.debug('is valid >>>> '+MyMatcher.matches());
            return MyMatcher.matches();
        }
        else{
            return true;
        }
    }
    

}