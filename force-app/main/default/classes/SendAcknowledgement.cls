public class SendAcknowledgement {
	//Use your Client Id
    String clientId ='3MVG9kBt168mda_9dSKrUDsDU6aDNA9CnoWvh6yoL4WGAlUuV4z37iz6AEbGtXwboq_382dYUtmUe5RZzCo8I';
    
    //Use your Client Secret
    String clientsecret='C5E5CCE370C4AAE0D512D8D3DA2A153AEFC7A1B1B2E29FF536244E963BC4261A';
    
    //Your Destination org username
    String username='piyush.pathak@silverlinecrm.com.hlsmaster';
    
    //Your Destination orgPassword+Security Token
    String password='mayday@4lGBq9d1ES89GYnQXgiA9akD4';//tQojXj5MrdTeHjv5SK0xsqaT1
    
    String accesstoken_url='https://login.salesforce.com/services/oauth2/token';
    
    String authurl='https://login.salesforce.com/services/oauth2/authorize';
    
    public class deserializeResponse {
        public String id;
        public String access_token;
    }
    
    public class requestWrapper{
        public String referralSFId;
        public String status;
        public String message;
        public requestWrapper(String name,String phone, String message)
        {
            this.referralSFId =name;
            this.status =phone;
            this.message = message;
        } 
    }
    
    public String ReturnAccessToken(SendAcknowledgement Acc){
        
        String reqbody = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+password;

        Http h= new Http();
        HttpRequest req= new HttpRequest();
        req.setBody(reqbody);
        System.debug('reqbody=='+reqbody);
        req.setMethod('POST');
        
        //Change “ap4” in url to your Destination Org Instance
        req.setEndpoint('https://hlsmasterorg.my.salesforce.com/services/oauth2/token');
                    
        HttpResponse res=h.send(req);
        
        System.debug(res.getBody()+'request body for access token');
        
        deserializeResponse resp1=(deserializeResponse)JSON.deserialize(res.getBody(),deserializeResponse.class);
        System.debug(resp1+'response for access token');
        
        return resp1.access_token;
                    
    }
    
     @future(callout=true)
    public static void respondToPCP(String sfId, String status, String message){
                        
        SendAcknowledgement objSendAck= new SendAcknowledgement();
        String accessToken=objSendAck.ReturnAccessToken(objSendAck);
        System.debug(accessToken+'===> access token');
        
        if(accessToken!=null){
            String endPoint='https://hlsmasterorg.my.salesforce.com/services/apexrest/v1/referralUpdate/';
            requestWrapper reqObj = new requestWrapper( sfId,status,message );
            String JSNBody = JSON.serialize(reqObj);
            
            Http h2= new Http();
            HttpRequest newreq= new HttpRequest();
            newreq.setHeader('Authorization','Bearer ' + accessToken);
            newreq.setHeader('Content-Type','application/json');
            newreq.setHeader('accept','application/json');
            newreq.setBody(JSNBody);
            newreq.setMethod('POST');
            newreq.setEndpoint(endPoint);
            
            HttpResponse res2=h2.send(newreq);
            
            deserializeResponse deresp=(deserializeResponse)System.JSON.deserialize(newreq.getBody(),deserializeResponse.class);
            
        }
        
    }
}