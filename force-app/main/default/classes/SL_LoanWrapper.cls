public class SL_LoanWrapper {

	public String applicationTakenMethodType;
	public List<Applications> applications;
	public Object fundingAmount;
	public Boolean lockField;

	public class Applications {
		public Borrower borrower;
		public Borrower coBorrower;
		public List<Employment> employment;
		public List<Residences> residences;
		public String propertyUsageType;
		public String totalIncomeAmount;
		public List<Income> income;
	}

	public class Employment {
		public String owner;
		public Boolean currentEmploymentIndicator;
		public String phoneNumber;
		public String addressCity;
		public String addressPostalCode;
		public String addressState;
		public String addressStreetLine1;
		public String employerName;
		public String endDate;
		public String positionDescription;
		public String startDate;


	}

	public class Borrower {
		public String birthDate;
		public String emailAddressText;
		public String firstName;
		public String homePhoneNumber;
		public String lastName;
	}

	public class Income {
		public String incomeType;
		public String owner;
		public String amount;
		public String currentIndicator;
	}

	public class Residences {
		public String addressPostalCode;
		public String addressState;
		public String addressCity;
		public String addressStreetLine1;
	}
	
	
	public static SL_LoanWrapper parse(String json) {
		return (SL_LoanWrapper) System.JSON.deserialize(json, SL_LoanWrapper.class);
	}
}