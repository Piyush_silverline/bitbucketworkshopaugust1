@isTest
global class SL_MockHttpResponseGeneratorToken implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {

    	HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{'+
					'	  "active": true,'+
					'	  "scope": "lp",'+
					'	  "client_id": "nurgbm0b",'+
					'	  "username": "abcmortgage@encompass:be79039931",'+
					'	  "token_type": "Bearer",'+
					'	  "exp": 1481704218,'+
					'	  "sub": "abcmortgage@encompass:be79039931",'+
					'	  "encompass_instance_id": "be79039931",'+
					'	  "user_name": "abcmortgage",'+
					'	  "user_key": "abcmortgage@encompass:be79039931",'+
					'	  "encompass_user": "Encompass\\be79039931\\abcmortgage",'+
					'	  "identity_type": "Enterprise",'+
					'	  "encompass_client_id": "3569564881",'+
					'	  "realm_name": "encompass:be79039931"'+
					'	}'
						);
        res.setStatusCode(204);
        return res;
    }
}