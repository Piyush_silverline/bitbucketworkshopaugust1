/**
* @ClassName    : SL_LoanPipeLineWrapper 
* @JIRATicket   :
* @CreatedOn    : 20 October 2018
* @CreatedBy    : Piyush Pathak
* @ModifiedBy   : 
* @Description  : By using this wrapper class we are creating the JSON Request for the Encompass Pipeline Integration.
*/
public class SL_LoanPipeLineWrapper {
    
	public Filter filter;
	public List<SortOrder> sortOrder;
	public List<String> fields;

	public class Filter {
		public String operator;
		public List<Terms> terms;
	}

	public class Terms {
		public String canonicalName;
		public String value;
		public String matchType;
		public Boolean include;
	}

	public class SortOrder {
		public String canonicalName;
		public String order;
	}

	
	public static SL_LoanPipeLineWrapper parse(String json) {
		return (SL_LoanPipeLineWrapper) System.JSON.deserialize(json, SL_LoanPipeLineWrapper.class);
	}
    
   
}