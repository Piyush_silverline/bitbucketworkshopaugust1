@isTest
global class SL_MockHttpResponseGeneratorEncompass implements HttpCalloutMock {

    global static Boolean isTestclass = false;
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        system.debug('inside the SL_MockHttpResponseGeneratorEncompass');
    	HTTPResponse res1 = new HTTPResponse();
    	 if (req.getEndpoint().endsWith('/loans?view=id&loanFolder=Prospects') && !isTestclass) {
                HTTPResponse res = new HTTPResponse();
                res.setHeader('Content-Type', 'application/json');
		        res.setBody('{"id":"testIdforencompass"}');
		        res.setStatusCode(201);
                return res;
            } 
            else if (req.getEndpoint().endsWith('/loans?view=id&loanFolder=Prospects') && isTestclass) {
                HTTPResponse res = new HTTPResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"id":"testIdforencompass"}');
                res.setStatusCode(400);
                return res;
            }
            else if (req.getEndpoint().endsWith('v1/token/introspection') && !isTestclass) {
                system.debug('inside the introspection endpoint');
                HTTPResponse res = new HTTPResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"active":false,"scope":"","client_id":"azlqyts","username":"salesforce@encompass:tebe11151441","token_type":"Bearer","exp":1537532464,"sub":"salesforce@encompass:tebe11151441","bearer_token":"TEBE11151441_6106a53a-9338-42e7-bb8f-973a76e600a4","encompass_instance_id":"TEBE11151441","user_name":"salesforce","user_key":"salesforce@encompass:tebe11151441","encompass_user":"EncompassEBE11151441alesforce","identity_type":"Enterprise","encompass_client_id":"3011151441","realm_name":"encompass:tebe11151441"}');
                res.setStatusCode(200);
                return res;
                
            }
            else if (req.getEndpoint().endsWith('v1/token/introspection') && isTestclass) {
                system.debug('inside the introspection endpoint');
                HTTPResponse res = new HTTPResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"active":true,"scope":"","client_id":"azlqyts","username":"salesforce@encompass:tebe11151441","token_type":"Bearer","exp":1537532464,"sub":"salesforce@encompass:tebe11151441","bearer_token":"TEBE11151441_6106a53a-9338-42e7-bb8f-973a76e600a4","encompass_instance_id":"TEBE11151441","user_name":"salesforce","user_key":"salesforce@encompass:tebe11151441","encompass_user":"EncompassEBE11151441alesforce","identity_type":"Enterprise","encompass_client_id":"3011151441","realm_name":"encompass:tebe11151441"}');
                res.setStatusCode(200);
                return res;
                
            }
            else if (req.getEndpoint().endsWith('oauth2/v1/token')) {
                system.debug('inside getting token');
                HTTPResponse res = new HTTPResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"access_token":"yUjSzsYY3QuNCbkwFnjPnbdwYWlK","token_type":"Bearer"}');
                res.setStatusCode(200);
                return res;
                
            }
            else if (req.getEndpoint().endsWith('/loanPipeline?limit=100')) {
                system.debug('inside getting token');
                String strResponse = '['+
'    {'+
'        "loanGuid": "a6117ca6-0898-45ca-89a8-377a4ef88898",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:00 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Person",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-56",'+
'            "Fields.1402": "8/26/2018 12:00:00 AM",'+
'            "Fields.1240": "banu_smg@yahoo.co.in",'+
'            "Fields.4004": "teet",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "test",'+
'            "Fields.97": "",'+
'            "Fields.1403": "8/26/2018 12:00:00 AM",'+
'            "Fields.1268": "banu_smg@yahoo.co.in",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "a6864c59-d303-4973-8fc7-d4cf7b3728e6",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:00 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Test PP",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-563-98",'+
'            "Fields.1402": "9/5/2018 12:00:00 AM",'+
'            "Fields.1240": "test.pp@account.com",'+
'            "Fields.4004": "Test",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "Person",'+
'            "Fields.97": "",'+
'            "Fields.1403": "8/26/2018 12:00:00 AM",'+
'            "Fields.1268": "test.person@account.",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "a7834fbe-ee62-4f65-955d-fc63b7446d64",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:01 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Test PP",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-563-98",'+
'            "Fields.1402": "9/5/2018 12:00:00 AM",'+
'            "Fields.1240": "test.pp@account.com",'+
'            "Fields.4004": "Test",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "Person",'+
'            "Fields.97": "",'+
'            "Fields.1403": "8/26/2018 12:00:00 AM",'+
'            "Fields.1268": "test.person@account.",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "a8062c23-209b-4fdd-8cd4-a8ec52b24fe9",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:03 AM",'+
'            "Fields.4000": "Loretta",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "McKee",'+
'            "Fields.65": "306-62-6376",'+
'            "Fields.66": "812-569-4625",'+
'            "Fields.1402": "3/31/1951 12:00:00 AM",'+
'            "Fields.1240": "crystal.feller@ucb.com",'+
'            "Fields.4004": "Crystal",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "Feller",'+
'            "Fields.97": "303-98-3167",'+
'            "Fields.1403": "10/20/1978 12:00:00 AM",'+
'            "Fields.1268": "crystal.feller@ucb.c",'+
'            "Fields.479": "Telephone",'+
'            "Fields.736": "5705.9700000000",'+
'            "Loan.LoanAmount": "70000.0000",'+
'            "Fields.MS.STATUS": "Received in UW",'+
'            "Fields.1172": "Conventional",'+
'            "Fields.19": "Cash-Out Refinance"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "aa77c9b3-9f31-4f00-959f-3201e2911441",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:04 AM",'+
'            "Fields.4000": "TestFirstName12345",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "TestLastName123450000",'+
'            "Fields.65": "",'+
'            "Fields.66": "415-555-1212",'+
'            "Fields.1402": "1/2/1983 12:00:00 AM",'+
'            "Fields.1240": "perf@home.com",'+
'            "Fields.4004": "TestFirstName212345",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "TestLastName212345999",'+
'            "Fields.97": "",'+
'            "Fields.1403": "1/4/1983 12:00:00 AM",'+
'            "Fields.1268": "M2@home.com",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "11112.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "ab9eef0b-2218-411e-84d6-9fbd2e39834e",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:05 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Person",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-56",'+
'            "Fields.1402": "8/26/2018 12:00:00 AM",'+
'            "Fields.1240": "test.person@account.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "Test PP",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "ab895b46-358c-41f1-b70e-e2aad14b1c0c",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:06 AM",'+
'            "Fields.4000": "Brian",'+
'            "Fields.4001": "Patrick",'+
'            "Fields.4002": "Griffin",'+
'            "Fields.65": "280-94-4574",'+
'            "Fields.66": "513-539-7068",'+
'            "Fields.1402": "8/26/1991 12:00:00 AM",'+
'            "Fields.1240": "bgriff25@gmail.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "3146.0000000000",'+
'            "Loan.LoanAmount": "68000.0000",'+
'            "Fields.MS.STATUS": "Reconciled",'+
'            "Fields.1172": "FHA",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "ad8aa853-96b6-46b2-9e9f-0bdecda3ba50",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:07 AM",'+
'            "Fields.4000": "TestFirstName12345",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "TestLastName123450000",'+
'            "Fields.65": "",'+
'            "Fields.66": "415-555-1212",'+
'            "Fields.1402": "1/2/1983 12:00:00 AM",'+
'            "Fields.1240": "perf@home.com",'+
'            "Fields.4004": "TestFirstName212345",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "TestLastName212345999",'+
'            "Fields.97": "",'+
'            "Fields.1403": "1/4/1983 12:00:00 AM",'+
'            "Fields.1268": "M2@home.com",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "11112.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "adacaf0b-fb53-4adf-b3f9-557074a24471",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:08 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Test PP",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-563-98",'+
'            "Fields.1402": "9/5/2018 12:00:00 AM",'+
'            "Fields.1240": "test.pp@account.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "adc6da0e-6d86-4b7f-b813-0af113c91180",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:09 AM",'+
'            "Fields.4000": "Ross",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Freddie",'+
'            "Fields.65": "991-20-0012",'+
'            "Fields.66": "765-960-0011",'+
'            "Fields.1402": "1/10/1990 12:00:00 AM",'+
'            "Fields.1240": "csims70@gmail.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "Telephone",'+
'            "Fields.736": "3000.0000000000",'+
'            "Loan.LoanAmount": "85000.0000",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "FarmersHomeA",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "ae654c34-c263-4d82-bca6-48a759d0d581",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:10 AM",'+
'            "Fields.4000": "",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "",'+
'            "Fields.65": "",'+
'            "Fields.66": "",'+
'            "Fields.1402": "",'+
'            "Fields.1240": "test@email.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "af10f0c3-9ae0-4a24-bab3-d6dd29334280",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:10 AM",'+
'            "Fields.4000": "",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "",'+
'            "Fields.65": "",'+
'            "Fields.66": "",'+
'            "Fields.1402": "",'+
'            "Fields.1240": "",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "af158106-458d-44c5-b90a-0aace92a7a1e",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:11 AM",'+
'            "Fields.4000": "rod",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "sneddon test",'+
'            "Fields.65": "",'+
'            "Fields.66": "",'+
'            "Fields.1402": "",'+
'            "Fields.1240": "",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "130500.0000",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "Conventional",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "afb84f4b-c8e4-45a2-a46b-d05f24b8b975",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:12 AM",'+
'            "Fields.4000": "",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "",'+
'            "Fields.65": "",'+
'            "Fields.66": "",'+
'            "Fields.1402": "",'+
'            "Fields.1240": "",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "HELOC",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b0db9c10-2287-4eb1-9ba7-86c6212c0a75",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:13 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Person",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-56",'+
'            "Fields.1402": "8/26/2018 12:00:00 AM",'+
'            "Fields.1240": "test.person@account.com",'+
'            "Fields.4004": "teet",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "test",'+
'            "Fields.97": "",'+
'            "Fields.1403": "8/26/2018 12:00:00 AM",'+
'            "Fields.1268": "hgjh@g.com",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b2f89626-bc6e-4e6d-a4d1-9ba488441a12",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:14 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "BorrowerP",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-563-98",'+
'            "Fields.1402": "9/5/2018 12:00:00 AM",'+
'            "Fields.1240": "test.pp@account.com",'+
'            "Fields.4004": "Test",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "CoPerson",'+
'            "Fields.97": "",'+
'            "Fields.1403": "8/26/2018 12:00:00 AM",'+
'            "Fields.1268": "test.person@account.",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b9fc95b4-2173-401e-a1df-b52367f2476c",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:15 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Person",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-56",'+
'            "Fields.1402": "8/26/2018 12:00:00 AM",'+
'            "Fields.1240": "test.person@account.com",'+
'            "Fields.4004": "teet",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "test",'+
'            "Fields.97": "",'+
'            "Fields.1403": "8/26/2018 12:00:00 AM",'+
'            "Fields.1268": "hgjh@g.com",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b386dea0-d77c-46a6-97df-b0bd6d93cd25",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:15 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Person",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-56",'+
'            "Fields.1402": "8/26/2018 12:00:00 AM",'+
'            "Fields.1240": "test.person@account.com",'+
'            "Fields.4004": "teet",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "test",'+
'            "Fields.97": "",'+
'            "Fields.1403": "8/26/2018 12:00:00 AM",'+
'            "Fields.1268": "hgjh@g.com",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b581e798-44dc-42c7-9c79-c46ec05a734f",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:16 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Person",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-56",'+
'            "Fields.1402": "8/26/2018 12:00:00 AM",'+
'            "Fields.1240": "banu_smg@yahoo.co.in",'+
'            "Fields.4004": "teet",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "test",'+
'            "Fields.97": "",'+
'            "Fields.1403": "8/26/2018 12:00:00 AM",'+
'            "Fields.1268": "hgjh@g.com",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b593cfb0-091d-45d0-813b-ace61aca1969",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:17 AM",'+
'            "Fields.4000": "",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "",'+
'            "Fields.65": "",'+
'            "Fields.66": "",'+
'            "Fields.1402": "",'+
'            "Fields.1240": "",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b739e4a4-013a-4453-8f25-79d2fec67831",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:18 AM",'+
'            "Fields.4000": "Elizabeth",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Freddie",'+
'            "Fields.65": "911-50-0015",'+
'            "Fields.66": "502-244-8357",'+
'            "Fields.1402": "1/1/2001 12:00:00 AM",'+
'            "Fields.1240": "gmail@gmail.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b901c42b-e1c3-4d20-87a5-fe05ef0e0748",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:19 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Test PP",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-563-98",'+
'            "Fields.1402": "9/5/2018 12:00:00 AM",'+
'            "Fields.1240": "test.pp@account.com",'+
'            "Fields.4004": "Test",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "Person",'+
'            "Fields.97": "",'+
'            "Fields.1403": "8/26/2018 12:00:00 AM",'+
'            "Fields.1268": "test.person@account.",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b1282b0c-d5fc-48e9-a70e-75c665f516cc",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:20 AM",'+
'            "Fields.4000": "Floyd",'+
'            "Fields.4001": "W",'+
'            "Fields.4002": "Foster",'+
'            "Fields.65": "291-54-3476",'+
'            "Fields.66": "513-543-1515",'+
'            "Fields.1402": "2/9/1958 12:00:00 AM",'+
'            "Fields.1240": "lisahuesman@gmail.com",'+
'            "Fields.4004": "Lisa",'+
'            "Fields.4005": "A",'+
'            "Fields.4006": "Huesman",'+
'            "Fields.97": "292-62-7055",'+
'            "Fields.1403": "6/15/1960 12:00:00 AM",'+
'            "Fields.1268": "lisahuesman@gmail.co",'+
'            "Fields.479": "Telephone",'+
'            "Fields.736": "24415.2500000000",'+
'            "Loan.LoanAmount": "102000.0000",'+
'            "Fields.MS.STATUS": "Funded",'+
'            "Fields.1172": "Conventional",'+
'            "Fields.19": "NoCash-Out Refinance"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b7689d6f-9f9d-4c9f-8b7c-347d3802a0ee",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:21 AM",'+
'            "Fields.4000": "",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "",'+
'            "Fields.65": "",'+
'            "Fields.66": "",'+
'            "Fields.1402": "",'+
'            "Fields.1240": "",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": "NoCash-Out Refinance"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b7756b90-ac7b-43f8-8d76-adddcff6eced",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:22 AM",'+
'            "Fields.4000": "",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "",'+
'            "Fields.65": "",'+
'            "Fields.66": "",'+
'            "Fields.1402": "",'+
'            "Fields.1240": "",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b3093612-6155-4286-ad1b-d992fbfb38b2",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:23 AM",'+
'            "Fields.4000": "Elizabeth",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Freddie",'+
'            "Fields.65": "991-50-0015",'+
'            "Fields.66": "812-427-3356",'+
'            "Fields.1402": "5/16/1985 12:00:00 AM",'+
'            "Fields.1240": "tsallen@mainsourcebank.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "b7538102-7140-4455-af13-b46eb9f82542",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:24 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Test PP",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-563-98",'+
'            "Fields.1402": "9/5/2018 12:00:00 AM",'+
'            "Fields.1240": "test.pp@account.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "bbfce4e9-a2d0-4531-a141-94ed9d5c0c86",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:25 AM",'+
'            "Fields.4000": "Mobile",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "ANDERSON",'+
'            "Fields.65": "",'+
'            "Fields.66": "415-555-1212",'+
'            "Fields.1402": "1/2/1983 12:00:00 AM",'+
'            "Fields.1240": "perf@home.com",'+
'            "Fields.4004": "M2",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "Perf2",'+
'            "Fields.97": "",'+
'            "Fields.1403": "1/4/1983 12:00:00 AM",'+
'            "Fields.1268": "M2@home.com",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "bfb31845-1241-4b4f-ad57-b680c08b8cd6",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:26 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Person",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-56",'+
'            "Fields.1402": "8/26/2018 12:00:00 AM",'+
'            "Fields.1240": "test.person@account.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "c6a68e21-3f95-441a-9b35-4dacca10489b",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:26 AM",'+
'            "Fields.4000": "Elizabeth",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Freddie",'+
'            "Fields.65": "991-50-0015",'+
'            "Fields.66": "502-555-1111",'+
'            "Fields.1402": "6/7/1990 12:00:00 AM",'+
'            "Fields.1240": "gpcooper@mainsourcebank.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "c8ab590a-c3bd-4549-8b4d-426ab2d6defe",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:28 AM",'+
'            "Fields.4000": "Kelly",'+
'            "Fields.4001": "S",'+
'            "Fields.4002": "Daniel",'+
'            "Fields.65": "305-86-9695",'+
'            "Fields.66": "219-576-3777",'+
'            "Fields.1402": "7/9/1970 12:00:00 AM",'+
'            "Fields.1240": "kellysdaniel07@gmail.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "Mail",'+
'            "Fields.736": "4142.6200000000",'+
'            "Loan.LoanAmount": "128000.0000",'+
'            "Fields.MS.STATUS": "Funded",'+
'            "Fields.1172": "Conventional",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "c55ce1e8-af07-47ae-9ef8-10d2a0483ca8",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:29 AM",'+
'            "Fields.4000": "TestFirstName12345",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "TestLastName12345",'+
'            "Fields.65": "",'+
'            "Fields.66": "415-555-1212",'+
'            "Fields.1402": "1/2/1983 12:00:00 AM",'+
'            "Fields.1240": "perf@home.com",'+
'            "Fields.4004": "TestFirstName212345",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "TestLastName212345",'+
'            "Fields.97": "",'+
'            "Fields.1403": "1/4/1983 12:00:00 AM",'+
'            "Fields.1268": "M2@home.com",'+
'            "Fields.479": "Internet",'+
'            "Fields.736": "11112.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "c89d75cd-7647-48df-a889-cd2cfe87e238",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:30 AM",'+
'            "Fields.4000": "Test",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "Person",'+
'            "Fields.65": "",'+
'            "Fields.66": "124-56",'+
'            "Fields.1402": "8/26/2018 12:00:00 AM",'+
'            "Fields.1240": "test.person@account.com",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": ""'+
'        }'+
'    },'+
'    {'+
'        "loanGuid": "c362a82a-5e76-41d0-b4c8-ff57063638d4",'+
'        "fields": {'+
'            "Loan.LastModified": "10/25/2018 5:04:30 AM",'+
'            "Fields.4000": "",'+
'            "Fields.4001": "",'+
'            "Fields.4002": "",'+
'            "Fields.65": "",'+
'            "Fields.66": "",'+
'            "Fields.1402": "",'+
'            "Fields.1240": "",'+
'            "Fields.4004": "",'+
'            "Fields.4005": "",'+
'            "Fields.4006": "",'+
'            "Fields.97": "",'+
'            "Fields.1403": "",'+
'            "Fields.1268": "",'+
'            "Fields.479": "",'+
'            "Fields.736": "0.0000000000",'+
'            "Loan.LoanAmount": "",'+
'            "Fields.MS.STATUS": "Started",'+
'            "Fields.1172": "",'+
'            "Fields.19": "Purchase"'+
'        }'+
'    }'+
']';
                HTTPResponse res = new HTTPResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody(strResponse);
                res.setStatusCode(200);
                return res;
                
            }
             else {
                System.assert(false, 'unexpected endpoint ' + req.getEndpoint());
            }
    	return res1;
    }
}