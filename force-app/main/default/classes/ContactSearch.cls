/*
 * CreatedBy:- Piyush
 * Created Date:- 22nd Jan, 2019
 * JIRA :- Trailhead task
 * Trailhead link:- https://trailhead.salesforce.com/content/learn/modules/apex_database/apex_database_soql?trailmix_creator_id=00550000006Iet7AAC&trailmix_id=pd1-preparation
 */
public class ContactSearch {
	
    public static List<Contact> searchForContacts(String strLastName, String strPostalCode){
        return [SELECT Id, Name FROM Contact WHERE LastName =:strLastName AND MailingPostalCode =:strPostalCode];
    }
}