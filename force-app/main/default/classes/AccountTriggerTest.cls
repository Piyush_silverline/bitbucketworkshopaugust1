@isTest
public class AccountTriggerTest {

    @testSetup
    static void testAccount(){
        List<Account> lstAccountToInsert = new List<Account>();
        for(Integer i=0; i<200; i++){
            Account objAccount = new Account(Name = 'Test '+i,
                                            BillingState = 'CA');
            lstAccountToInsert.add(objAccount);
            
        }
        insert lstAccountToInsert;
        
        for(Account objAcc : [SELECT Id, Name, ShippingState FROM Account WHERE BillingState = 'CA']){
            system.assertEquals(objAcc.ShippingState, 'CA');
        }
    }
    
    @isTest
    static void testestmethod(){
        
    }
}