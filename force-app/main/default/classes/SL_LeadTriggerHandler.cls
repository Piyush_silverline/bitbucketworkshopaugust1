public class SL_LeadTriggerHandler {
	
    public static void sendAcknowledgementForReferral(List<Lead> lstNewLeads, Map<Id,Lead> mapOldLeads){
        for(Lead objLead : lstNewLeads){
            if(objLead.Status != mapOldLeads.get(objLead.Id).Status && objLead.Status != null && lstNewLeads.size() ==1 && objLead.Referral_SF_Id__c != null){
                SendAcknowledgement.respondToPCP(objLead.Referral_SF_Id__c, objLead.Status,objLead.Message_To_PCP__c);
            }
        }
        
    }
}