/*
 * CreatedBy:- Piyush
 * Created Date:- 22nd Jan, 2019
 * JIRA :- Trailhead task
 * Trailhead link:- https://trailhead.salesforce.com/content/learn/modules/visualforce_fundamentals/visualforce_custom_controllers?trailmix_creator_id=00550000006Iet7AAC&trailmix_id=pd1-preparation
 */
public class NewCaseListController {
	
    public List<Case> getNewCases() {
    
   /* List<Contact> results = Database.query(
        'SELECT Id, FirstName, LastName, Title, Email ' +
        'FROM Contact ' +
        'ORDER BY ' + sortOrder + ' ASC ' +
        'LIMIT 10'
    );*/
    return [SELECT Id, CaseNumber FROM Case WHERE Status = 'New'];
}
}