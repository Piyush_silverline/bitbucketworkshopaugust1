@RestResource(urlMapping='/v1/createLeads/*')
global class SL_CreateReferralLead {
	@HttpPost
    global static String doPost() {
         RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response;
        string jsonString=req.requestBody.tostring();
        System.debug('JsonString-->>'+jsonString);
        //String jsonString = '{"caseId":"5004x000008Ofu9AAC","patientDetails":{"FirstName":"Boris","LastName":"Lowe","Name":"Boris Lowe","PersonBirthdate":"1957-02-26","cern__HiRace__pc":"Asian","cern__HiEthnicity__pc":"Not Hispanic nor Latino","HealthCloudGA__PrimaryLanguage__pc":"English","HealthCloudGA__SecondaryLanguage__pc":"English","Phone":"483-531-2250","PersonMailingAddress":{"city":"Springfield","country":"United States","geocodeAccuracy":null,"latitude":null,"longitude":null,"postalCode":null,"state":null,"street":"P.O. Box 114, 1713 Tellus Avenue"},"PersonEmail":"eu.dolor@nonluctussit.com","Id":"0014x00000HMbz8AAD","Guardian_Name__c":"TEST Guardian","Guardian_Relationship_to_Patient__c":"Son"},"relatedRecords":{"CareBarrier":"TEST Care Barrier, Test Care Barrier2","CoverageBenefit":"TEST CoverageBenefit, Test CoverageBenefit2","CoverageBenefitItem":"TEST CoverageBenefitItem, Test CoverageBenefitItem2","CareDeterminant":"TEST CareDeterminant, Test CareDeterminant2","MemberPlan":"TEST MemberPlan, Test MemberPlan2"},"additionalDetails":{"Description_of_what_prompted_the_referral":"data 4","Specify_referring_provider_notes":"data 4","Specify_the_specific_ICD10_CM_Diagnosis_Code":"data 3","Specify_Facility_Information_optional":"data 2","Specify_Specialty_needed_for_Referral":"data 1","Referred_To__c":"0014x00000HMbz8AAD","PCP__c":"0014x00000eRTnUAAW"}}';
        Map<String,object> mapJSON = (Map<String,object>)JSON.deserializeUntyped(jsonString);
        system.debug('patient details=='+mapJSON.get('patientDetails'));
        Map<String,object> mapPatientdetails = (Map<String,object>)mapJSON.get('patientDetails');
        Map<String,object> mapRelatedRecords = (Map<String,object>)mapJSON.get('relatedRecords');
        Map<String,object> mapadditionalDetails = (Map<String,object>)mapJSON.get('additionalDetails');
        System.debug('FirstName=='+mapPatientdetails.get('FirstName'));
        Lead objLead = new Lead(Status = 'Open - Not Contacted',Company = 'PCP Company', Referral_SF_Id__c = getString(mapJSON.get('caseId')), FirstName = getString(mapPatientdetails.get('FirstName')) , LastName = getString(mapPatientdetails.get('LastName')),  Race__c = getString(mapPatientdetails.get('cern__HiRace__pc')),Primary_Language__c = getString(mapPatientdetails.get('HealthCloudGA__PrimaryLanguage__pc')) , MobilePhone =getString(mapPatientdetails.get('Phone')) ,
                       		Email = getString(mapPatientdetails.get('PersonEmail')), Guardian_Name__c = getString(mapPatientdetails.get('Guardian_Name__c')), Guardian_Relationship__c = getString(mapPatientdetails.get('Guardian_Relationship_to_Patient__c')),
                            CareBarrier__c = getString(mapRelatedRecords.get('CareBarrier')),CoverageBenefit__c = getString(mapRelatedRecords.get('CoverageBenefit')),CoverageBenefitItem__c= getString(mapRelatedRecords.get('CoverageBenefitItem')),CareDeterminant__c= getString(mapRelatedRecords.get('CareDeterminant')), MemberPlan__c = getString(mapRelatedRecords.get('MemberPlan')), 
                            Referral_Description__c = getString(mapadditionalDetails.get('Description_of_what_prompted_the_referral')), PCP_Notes__c = getString(mapadditionalDetails.get('Specify_referring_provider_notes')), Diagnosis_Code__c =getString(mapadditionalDetails.get('Specify_the_specific_ICD10_CM_Diagnosis_Code')) , Provider__c= getString(mapadditionalDetails.get('Referred_To__c')), PCP__c = getString(mapadditionalDetails.get('PCP__c')));


       
        insert objLead;
        return objLead.Id;
        
    }
    public static String getString(object obj){
        return String.valueOf(obj);
    }
    /*public class responseWrapper{
        public string wName;
        public string wPhone;
    }*/
    /* responseWrapper wResp=(responseWrapper) JSON.deserialize(jsonString,responseWrapper.class);
        System.debug('WResp-->>'+wResp);
        System.debug('inside the dopost method');
        Lead objLead = new Lead(FirstName = 'TEst',LastName = wResp.wName, Company = wResp.wPhone, Status = 'Open - Not Contacted');*/
    
}