/***
* @ClassName    : SL_CustomException 
* @JIRATicket   : 
* @CreatedOn    : 11 Sep, 2018 
* @CreatedBy    : Piyush Pathak
* @ModifiedBy   : 
* @Description  : Utility class to store all the Exceptions.
*/
public class SL_CustomException extends Exception {

	// List of static error strings
    public static String API_INTEGRATION_EXCEPTION =  'API Callout failed calling endPointURl ={0}&the Status_code ={1}&Status_Message={2}';
    public static String MORE_THAN_ONE_LOAN_FOR_ENCOMPASS = 'Please update only one record for Loan Integration to Encompass';
    public static String ERROR_FOR_WRONG_ENCOMPASS_DATA = 'Either FirstName, LastName,Email AND/OR BirthDate is missing on the Account Record ';
}