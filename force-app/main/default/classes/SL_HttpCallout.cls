/**
* @ClassName    : SL_HttpCallout 
* @JIRATicket   : Reusable Http class
* @CreatedOn    : 11th September,2018
* @CreatedBy    : Banu Ponnuswamy
* @ModifiedBy   : 
* @Description  : HTTP util class for making callouts. It has callout methods for authentication and getting access tokens, and api service callouts.
*/
public class SL_HttpCallout {
    private static final Integer TIME_OUT       =  6000;
    public static final String  POST_METHOD    = 'POST';
    public static final String  GET_METHOD     = 'GET';
    // private final static String  AUTHENTICATION_REQUEST_BODY = 'grant_type={0}&client_id={1}&client_secret={2}&username={3}&password={4}';
    private final static String  ACCESS_TOKEN_REQUEST_BODY = 'grant_type={0}&username={1}&password={2}';
    private final static String  CHECK_ACCESS_TOKEN_REQUEST_BODY = 'token={0}';
    
    public static HttpResponse httpCallForAuthentication (API_Settings__c apiSettingObj) {
        return httpCallForAuthentication (apiSettingObj.Token_URL__c, apiSettingObj.grant_type__c, apiSettingObj.Client_ID__c, apiSettingObj.Client_Secret__c, apiSettingObj.UserName__c, apiSettingObj.Password__c);
    }
    
    public static HttpResponse httpCallForAuthentication (String endPointURl, String grantType, String clientId, String clientSecret, String userName, String password) {
        String requestBody = String.format(ACCESS_TOKEN_REQUEST_BODY ,new string[]{grantType,userName,password});
        return getHttpResponse (endPointURl, buildAuthHeader(clientId,clientSecret), POST_METHOD, requestBody);
    }
    
    public static HttpResponse httpCallForCheckingAuthTokenExpiry (API_Settings__c apiSettingObj) {
        return httpCallForCheckingAuthTokenExpiry (apiSettingObj.Token_Introspection_URL__c,apiSettingObj.Client_ID__c, apiSettingObj.Client_Secret__c, apiSettingObj.Access_Token__c);
    }
    
    public static HttpResponse httpCallForCheckingAuthTokenExpiry (String endPointURl, String clientId, String clientSecret, String accessToken) {
        String requestBody = String.format(CHECK_ACCESS_TOKEN_REQUEST_BODY ,new string[]{accessToken});
        return getHttpResponse (endPointURl, buildAuthHeader(clientId,clientSecret), POST_METHOD, requestBody);
    }    
    
     public static HttpResponse httpCallAPIServiceCall (SL_OAuthController.OAuthDetails oAuth, String requestBody, String methodType) {
        String authorizationHeader = oAuth.token_type+':'+oAuth.access_token;
        return getHttpResponse (oAuth.instance_url, authorizationHeader, methodType, requestBody);
    } 
    
    //Method to make the HTTP callout based on the arguments provided
    public static HttpResponse getHttpResponse (String endPointURl, String authorizationHeader, String methodType, String requestBody) {
        try{
            HttpRequest req = new HttpRequest();
            system.debug('endpoint for the integration is >>>'+endPointURl);
            req.setEndpoint(endPointURl);
            req.setMethod(methodType);
            if(NULL != authorizationHeader)
                req.setHeader('Authorization', authorizationHeader);
            if(NULL != requestBody)
            	req.setBody(requestBody);
            req.setTimeout(TIME_OUT);
            Http http = new Http();
            HttpResponse res = http.send(req);
            System.debug('res--'+res.getStatusCode());
            System.debug('res Body--'+res.getBody());
            if(res.getStatusCode() != NULL && !isSucessfulResponse(res.getStatusCode()))
                  throw new SL_FFBCustomException(String.format(SL_FFBCustomException.API_INTEGRATION_EXCEPTION ,new string[]{endPointURl,String.valueOf(res.getStatusCode()),res.getStatus()+res.getBody()}));
            return res;
        } catch (CalloutException exp){
            System.debug('inside exception');
            system.debug(exp.getMessage());
            throw exp;//implement custom exception
        }
    }
    
    private static String buildAuthHeader (String paramId, String paramSecret) {
        Blob headerValue = Blob.valueOf( paramId + ':' +  paramSecret);
        return 'Basic ' + EncodingUtil.base64Encode(headerValue);
    }

    private static Boolean isSucessfulResponse(Integer statusCode) {
        if(statusCode == 200 || statusCode == 201 ||  statusCode == 202 || statusCode == 204 || statusCode == 304){
            return true;
        }
        return false;
    }
}