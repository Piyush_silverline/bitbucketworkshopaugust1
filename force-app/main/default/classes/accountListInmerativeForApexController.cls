public with sharing class accountListInmerativeForApexController {

    @AuraEnabled
    public static List<String> getaccountList(Integer numberofRecords){
        List<String> lstString = new List<String>();
        for(Account objAccount : [SELECt Id, Name, Phone FROM Account LIMIT : numberofRecords]){
            lstString.add(objAccount.name+'$$'+objAccount.Phone+'$$'+objAccount.Id);
        }
        return lstString;
    }
}