/**
* @ClassName    : SL_Encompass_JSON_Parser 
* @JIRATicket   :
* @CreatedOn    : 24 October 2018
* @CreatedBy    : Banu Ponnuswamy(Silverline)
* @ModifiedBy   : Piyush Pathak
* @Description  : JSON parser class to parse the Encompass JSON request/responses and create the Sobjects based on API_JSON_Object_Mappping__mdt
*/
public class SL_Encompass_JSON_Parser {
    Map <String, Map<String, API_JSON_Object_Mappping__mdt>> entityJsonSFObjectFieldMap = new Map <String, Map<String, API_JSON_Object_Mappping__mdt>>(); 
    Map <String, String> entityObjectMap = new Map <String, String>();
    Map <String, List <String>> entityJsonFieldListMap = new Map <String, List <String>>();
    Map <String, Boolean> fieldHasChild = new Map <String, Boolean>();
    //Complete list of loans and json object
    public Map<String, Map<String,Object>> loanIdFieldSetMap = new Map<String, Map<String,Object>>();
    //Maps will get loaded for database insert/updates
    public Map <String, sObject> borrowerAccountMap = new  Map <String, sObject>();
    public Map <String, sObject> coBorrowerAccountMap = new  Map <String, sObject>();
    public Map <String, sObject> loanOptyMap = new  Map <String, sObject>();
    public static List<String> lstEncompassFields = new List<String>();
    public static string strEncompassLastModified ; 
    
    public SL_Encompass_JSON_Parser (){
        loadMaps();
    }
    
    //Method orchestrates all the flow
    public void parseEncompassReponse (String jsonString){
        jsonParser(jsonString);
        buildSFObjectRecordList();
    }
    
    //Current code will not work if 2 list has same child name. We are getting all the JSON fields flat in one map
    private  void jsonParser(String jsonString){
        List<Object> results = (List<Object>)JSON.deserializeUntyped(jsonString);
        Map<String, Object> fieldValueMap;
        Map<String, Object>  jsonObjDataMapforSetting = (Map<String, Object>)results.get(results.size()-1);
        Map<String, Object> mapFieldValue = (Map<String, Object>)jsonObjDataMapforSetting.get('fields');
        strEncompassLastModified = String.valueOf(mapFieldValue.get('Loan.LastModified'));
        
        //Current code will not work if 2 list has same child name
        for(integer i=0; i<results.size(); i++){
            String mapKey = '';
            Map<String, Object>  jsonObjDataMap = (Map<String, Object>)results.get(i);
            fieldValueMap  = new Map <String, Object>();
            for(String jsonObjectName: jsonObjDataMap.keySet()){
                if(this.fieldHasChild.get(jsonObjectName.toUpperCase()) <> NULL && !this.fieldHasChild.get(jsonObjectName.toUpperCase())){
                    fieldValueMap.put(jsonObjectName.toUpperCase(),jsonObjDataMap.get(jsonObjectName));
                }else {
                    getChildRecords((Map<String, Object>)jsonObjDataMap.get(jsonObjectName),fieldValueMap);//First level parent-children
                    //Check for children with grand children
                    for(String childObjName :fieldValueMap.keySet()){
                        if(this.fieldHasChild.get(childObjName.toUpperCase()) <> NULL && this.fieldHasChild.get(childObjName.toUpperCase())){
                            getChildRecords((Map<String, Object>)fieldValueMap.get(childObjName.toUpperCase()),fieldValueMap); 
                            fieldValueMap.remove(childObjName.toUpperCase());
                        }
                    }
                }
            }
            mapKey = (String) fieldValueMap.get('LOANGUID'); //THis can be configurable
            loanIdFieldSetMap.put(mapKey,fieldValueMap);
        }
    }
    
    //Method to parse the JSON child elements
    private static  void getChildRecords(Map<String,Object> childDataMap, Map<String, Object> fieldValueMap) {
        
        for(String childObjName: childDataMap.keySet()){
            fieldValueMap.put(childObjName.toUpperCase(), childDataMap.get(childObjName));
        }
    }
    
    private sObject setValuesToSFFields(Map<String,Object> jsonFieldValue, String entity){
        Map<String, API_JSON_Object_Mappping__mdt> jsonFieldSFObjMtdMap = this.entityJsonSFObjectFieldMap.get(entity);
        sObject sObj = Schema.getGlobalDescribe().get(entityObjectMap.get(entity)).newSObject() ;
        for(String fieldName : jsonFieldSFObjMtdMap.keySet()){
            if(jsonFieldValue.get(fieldName) <> NULL && String.isNotBlank((String)jsonFieldValue.get(fieldName))){
                switch on jsonFieldSFObjMtdMap.get(fieldName).SF_Field_Type__c.toUpperCase(){
                    when 'STRING' {
                        sObj.put(jsonFieldSFObjMtdMap.get(fieldName).SF_Field_Name__c, (String)jsonFieldValue.get(fieldName)); 
                    }
                    when 'INTEGER' {
                        sObj.put(jsonFieldSFObjMtdMap.get(fieldName).SF_Field_Name__c, Integer.valueof(jsonFieldValue.get(fieldName))); 
                    }
                    when 'DATE' {
                        sObj.put(jsonFieldSFObjMtdMap.get(fieldName).SF_Field_Name__c, Date.valueof(convertDate((String)jsonFieldValue.get(fieldName)))); 
                    }
                    when 'CURRENCY' {
                        String strValue = String.valueOf(jsonFieldValue.get(fieldName));
                        sObj.put(jsonFieldSFObjMtdMap.get(fieldName).SF_Field_Name__c, Decimal.valueof(strValue)); 
                    }
                    when else {
                        sObj.put(jsonFieldSFObjMtdMap.get(fieldName).SF_Field_Name__c, NULL); 
                    }
                }
            }
            
        }
        return sObj;
    }
    
    private void buildSFObjectRecordList(){
        for(String loanId : loanIdFieldSetMap.keySet()){
            this.borrowerAccountMap.put(loanId,setValuesToSFFields(loanIdFieldSetMap.get(loanId),'Borrower'));
            this.coBorrowerAccountMap.put(loanId,setValuesToSFFields(loanIdFieldSetMap.get(loanId),'Co-Borrower')); 
            this.loanOptyMap.put(loanId,setValuesToSFFields(loanIdFieldSetMap.get(loanId),'Loan'));
        }
    }
    
    //Load all the values from Custom Metadata    
    private void loadMaps() {
        for(API_JSON_Object_Mappping__mdt apiMetadaObj :[SELECT MasterLabel,Entity_Name__c,Has_child__c,JSON_Object__c,Order_Sequence__c,Service__c,SF_Field_Name__c,SF_Object__c,SF_Field_Type__c FROM API_JSON_Object_Mappping__mdt where API__C ='Encompass' and Service__C='loanPipeline' order by Order_Sequence__c]) {
            //List to Get all the fields needed for Json Request.
            lstEncompassFields.add(apiMetadaObj.MasterLabel);
            if(apiMetadaObj.Entity_Name__c <> NULL){
                //Load JSON fields to metadata
                if(!this.entityJsonSFObjectFieldMap.containsKey(apiMetadaObj.Entity_Name__c)) {
                    this.entityJsonSFObjectFieldMap.put(apiMetadaObj.Entity_Name__c, new Map <String, API_JSON_Object_Mappping__mdt>{apiMetadaObj.JSON_Object__c.toUpperCase() => apiMetadaObj} );
                } else {
                    this.entityJsonSFObjectFieldMap.get(apiMetadaObj.Entity_Name__c).put(apiMetadaObj.JSON_Object__c.toUpperCase(),apiMetadaObj); 
                }
                //Load Entiry to SF obj
                if(!this.entityObjectMap.containsKey(apiMetadaObj.Entity_Name__c)) {
                    this.entityObjectMap.put(apiMetadaObj.Entity_Name__c,apiMetadaObj.SF_Object__c);
                }
                //Load entity to SF fieldList
                if(!this.entityJsonFieldListMap.containsKey(apiMetadaObj.Entity_Name__c)){
                    this.entityJsonFieldListMap.put(apiMetadaObj.Entity_Name__c, new List<String>{apiMetadaObj.JSON_Object__c.toUpperCase()});
                }else {
                    this.entityJsonFieldListMap.get(apiMetadaObj.Entity_Name__c).add(apiMetadaObj.JSON_Object__c.toUpperCase());
                }
            } 
            String strJSONObject = apiMetadaObj.JSON_Object__c;
            strJSONObject = strJSONObject.toUpperCase();
            this.fieldHasChild.put(strJSONObject,apiMetadaObj.Has_child__c);
        }
    }
    
    private static String convertDate(String dateStr){
        Date dt = date.parse(dateStr.split(' ')[0]);
        String convertedDate = DateTime.newInstance(dt.year(), dt.month(), dt.day()).format('yyyy-MM-dd');
        return convertedDate;
    }
    
}