@isTest
private class SL_HttpCalloutandOAuth_Test {

	@testSetup static void setupMethod() {

		API_Settings__c objApiSettiting = new API_Settings__c(Name = 'Encompass',Access_Token__c = 'tYF4fYzIko0vimK0WWpdHqJkf3nd',Client_ID__c = 'azlqytf', Client_Secret__c = '*vduV8n$oK&S^U!Z6spiBUSV69k717W49E0iTietJ**D5ANurd5Cm2Kii2s', End_Point_URL__c	 = 'https://api.elliemae.com/encompass/v1',
        														Grant_Type__c = 'password', Password__c = 'Mortgage@11', Scope__c = '', Token_Type__c = 'Bearer', Token_Introspection_URL__c = 'https://api.elliemae.com/oauth2/v1/token/introspection',
        														Token_URL__c = 'https://api.elliemae.com/oauth2/v1/token',UserName__c = 'salesforce@encompass:TEBE11151441');
        insert objApiSettiting;
	}

	@isTest static void testCallout() {

		Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new SL_MockHttpResponseGeneratorEncompass());
		
        API_Settings__c objCustomSetting = [SELECT Id, Name FROM API_Settings__c WHERE Name = 'Encompass' LIMIT 1];
        system.debug('value of the settin >'+objCustomSetting);
        
        try{
        	SL_OAuthController objOAuthForCustomSetting = new SL_OAuthController(objCustomSetting);
        }
        catch(Exception ex) {
            system.assert(ex.getMessage().contains('API Settings object cannot be NULL'));
        }
        
        try{
            String str = NULL;
        	SL_OAuthController objOAuthForCustomSetting = new SL_OAuthController(str);
        }
        catch(Exception ex) {
            system.assert(ex.getMessage().contains('API Name cannot be NULL'));
        }
        
        SL_OAuthController objOAuth = new SL_OAuthController('Encompass');
        
        
        
        
        SL_OAuthController.OAuthDetails  objwrapper = new SL_OAuthController.OAuthDetails();
        objwrapper = objOAuth.getToken();
        //SL_HttpCallout.httpCallAPIServiceCall(objwrapper,'sampleJsonBody', 'samplemethodtype');
        SL_MockHttpResponseGeneratorEncompass.isTestclass = true;
        objwrapper = objOAuth.getToken();
        //SL_OAuthController.setTokenInApiSettings('Encompass', 'abc');
        objwrapper.isTokenUpdated = true;
        objwrapper.apiName = 'Encompass';
        objOAuth.updateOauthSettingsInSF();
        
        Test.stopTest();
	}
    
    @isTest static void testCallout2() {
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new SL_MockHttpResponseGeneratorEncompass());
        SL_OAuthController.OAuthDetails  objwrapper = new SL_OAuthController.OAuthDetails();
        objwrapper.instance_url = 'https://api.elliemae.com/encompass/v1/loans?view=id&loanFolder=Prospects';
        SL_MockHttpResponseGeneratorEncompass.isTestclass = true;
        try{
        	SL_HttpCallout.httpCallAPIServiceCall(objwrapper, 'sampleJsonBody', 'samplemethodtype');
        }
        catch(Exception ex) {
            system.assert(ex.getMessage().contains('API Callout failed calling endPointURl'));
        }
        Test.stopTest();
    }
}