/**
* @ClassName    : SL_OAuthController 
* @JIRATicket   : Reusable OAuth controller class
* @CreatedOn    : 11th September,2018
* @CreatedBy    : Banu Ponnuswamy
* @ModifiedBy   : 
* @Description  : This class has all the OAuth authentication functions such as get access token, check for the token expiry, login to API. API_Settings__c 
                  custom object needs to be configured with API details to run this class.
*/
public class SL_OAuthController {
    API_Settings__c apiSettingObj;
    OAuthDetails oAuthObj = NULL;
    public SL_OAuthController (String oAuthAPIName) {
        if(NULL == oAuthAPIName)
             throw new SL_FFBCustomException('API Name cannot be NULL');
         
        this.apiSettingObj = [SELECT Grant_Type__c, Access_Token__c,Client_ID__c,Client_Secret__c,End_Point_URL__c,Id,Name,Password__c,
                         Scope__c,Token_Introspection_URL__c,Token_Type__c,Token_URL__c,UserName__c FROM API_Settings__c where Name = :oAuthAPIName limit 1 ];
    }

    public SL_OAuthController ( API_Settings__c apiSettingObject) {
        if(NULL == apiSettingObj)
           throw new SL_FFBCustomException('API Settings object cannot be NULL');
            this.apiSettingObj = apiSettingObject;
    }
    
    //Method to get OAuth Access token.Check the token stored in custom settings is active if not pull the active token from the authentication server
    public OAuthDetails getToken(){
        SYstem.debug('apiSettingObj--'+this.apiSettingObj);
                //Check if the existing token is active
            if(NULL != this.apiSettingObj.Access_Token__c && isTokenActive()){
                oAuthObj = new OAuthDetails();
                this.oAuthObj.access_token = this.apiSettingObj.Access_Token__c;
                this.oAuthObj.instance_url = this.apiSettingObj.End_Point_URL__c;
                this.oAuthObj.token_type =   this.apiSettingObj.Token_Type__c;
                this.oAuthObj.isTokenUpdated = false;
                System.debug('###API TOKEN IN SF IS ACTIVE ###');
            }else {
                System.debug('###Calling API TO GET NEW TOKEN ###');
                this.oAuthObj = (OAuthDetails)JSON.deserialize(SL_HttpCallout.httpCallForAuthentication(this.apiSettingObj).getBody(), OAuthDetails.class); 
                this.oAuthObj.instance_url = this.apiSettingObj.End_Point_URL__c;
                this.oAuthObj.apiName = this.apiSettingObj.Name;
                this.apiSettingObj.Access_Token__c = oAuthObj.access_token;
                this.oAuthObj.isTokenUpdated = true;
            }
            return this.oAuthObj;
    }

    //Call this method after completing the OAuth authentication to update the OAuth information in the custom settings
    public void updateOauthSettingsInSF() {
      if(this.oAuthObj.isTokenUpdated) {
         setTokenInApiSettings(this.oAuthObj.apiName, this.oAuthObj.access_token);
      }
    }

    //Method to update the token in the custon settings - set as future because this should not impact the current thread for invoking the API
    //@future
    public static void setTokenInApiSettings(String apiName, String token){
         API_Settings__c apiSettingToUpdateObj = [SELECT  Access_Token__c, Name FROM API_Settings__c where Name = :apiName limit 1 ];
         apiSettingToUpdateObj.Access_Token__c = token;
         update apiSettingToUpdateObj;
        System.debug('###ACCESS-TOKEN UPDATED FOR '+apiName+'###');
    }
    
    //Method to check if the token is active
    public Boolean isTokenActive () {
        Boolean isTokenActiveFlag = false;
        try{
            OAuthDetails oAuthCheckObj = (OAuthDetails)JSON.deserialize( SL_HttpCallout.httpCallForCheckingAuthTokenExpiry(this.apiSettingObj).getBody(), OAuthDetails.class); 
            isTokenActiveFlag = oAuthCheckObj.active;
        }catch(Exception e) {
            isTokenActiveFlag = false;
            System.debug('Token expired'+e.getMessage());
        }
        return isTokenActiveFlag;
    }
    
    //inner class to store Oauth details
    public class OAuthDetails{
        public String  apiName{get;set;} 
        public String  instance_url{get;set;}
        public String  access_token{get;set;}    
        public String  token_type{get;set;}
        public Boolean active {get;set;}
        public Boolean isTokenUpdated {get;set;}
    }    
}