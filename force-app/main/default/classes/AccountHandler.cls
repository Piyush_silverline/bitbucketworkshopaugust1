/*
 * CreatedBy:- Piyush
 * Created Date:- 22nd Jan, 2019
 * JIRA :- Trailhead task
 * Trailhead link:- https://trailhead.salesforce.com/content/learn/modules/apex_database/apex_database_dml?trailmix_creator_id=00550000006Iet7AAC&trailmix_id=pd1-preparation
 */
public class AccountHandler {
	
    public static Account insertNewAccount(String strAccountName){
        Account objAccount = new Account(Name = strAccountName);
        try{
            insert objAccount;
            return objAccount;
        }
        catch(Exception ex){
        	return null;
        }
    }
}