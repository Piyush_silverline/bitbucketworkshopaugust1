/*
* CreatedBy:- Piyush
* Created Date:- 22nd Jan, 2019
* JIRA :- Trailhead task
* Trailhead link:- https://trailhead.salesforce.com/content/learn/projects/quickstart-apex/quickstart-apex-1?trailmix_creator_id=00550000006Iet7AAC&trailmix_id=pd1-preparation
*/
public class OlderAccountsUtility {
    /*
     * method name :- updateOlderAccounts
     * trailhead link:- https://trailhead.salesforce.com/content/learn/projects/quickstart-apex/quickstart-apex-2?trailmix_creator_id=00550000006Iet7AAC&trailmix_id=pd1-preparation
	*/
    public static void updateOlderAccounts() {
        // Get the 5 oldest accounts
        Account[] oldAccounts = [SELECT Id, Description FROM Account ORDER BY CreatedDate ASC LIMIT 5];
        // loop through them and update the Description field
        for (Account acct : oldAccounts) {
            acct.Description = 'Heritage Account';
        }
        // save the change you made
        update oldAccounts;
    }
    
}