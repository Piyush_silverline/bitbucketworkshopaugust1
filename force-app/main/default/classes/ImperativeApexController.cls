public with sharing class ImperativeApexController {
    public ImperativeApexController() {

    }
    @AuraEnabled
    public static List<String> getAccountList(Integer numberofRecords){
        List<String> lstString = new List<String>();
        for(Account objAccount : [SELECt Id, Name, Phone FROM Account LIMIT : numberofRecords]){
            lstString.add(objAccount.name+'$$'+objAccount.Id+'$$'+objAccount.Phone);
        }
        return lstString;
    }
}