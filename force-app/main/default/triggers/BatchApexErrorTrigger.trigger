trigger BatchApexErrorTrigger on BatchApexErrorEvent (after insert) {
	//
	//BatchApexErrorTriggerHelper.afterInsert(Trigger.newMap);
	List<BatchLeadConvertErrors__c > lstErrorRecordsToInsert = new List<BatchLeadConvertErrors__c >();
        for(BatchApexErrorEvent objErrorEvent : Trigger.new){//Map.values()
            BatchLeadConvertErrors__c objConvertError = new BatchLeadConvertErrors__c();
            objConvertError.AsyncApexJobId__c = objErrorEvent.AsyncApexJobId;
            objConvertError.Records__c = objErrorEvent.JobScope;
            objConvertError.StackTrace__c = objErrorEvent.StackTrace;
            lstErrorRecordsToInsert.add(objConvertError);
        }
        insert lstErrorRecordsToInsert;
}